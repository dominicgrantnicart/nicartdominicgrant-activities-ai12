import {SafeAreaview,StyleSheet,TextInput, Button, Alert } from 'react-native';

  
export default function App (){

const [text, onChangeText]=React.useState('Enter A Text');
  
  return (
<SafeAreaview style={styles.container}>
<TextInput style= {styles.input}
onChangeText={onChangeText}
value={text}
/>
<Button color="blue"
  title= "Submit"
  onPress={()=>Alert.alert('Alert','You just typed: "' + text + '"')}
/>
</SafeAreaview>
);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input:{
    borderWidth:1,
    borderColor:'#777',
    padding:8,
    margin:10,
    width:200,
  },
});


